Resources:
1. https://cliutils.gitlab.io/modern-cmake/chapters/basics/functions.html
1. https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/

Commands:
cmake -S . -B build -DBUILD_SHARED_LIBS=1 && cmake --build build && cd build
cmake -LAH
cmake -LH
ldd EXE_NAME or Lib Name
objdump -x EXE or Lib Name