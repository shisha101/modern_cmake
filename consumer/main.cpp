#include <string>
#include <iostream>
#include <my_test_lib/dummy_obj_1.h>
#include <my_test_lib/dummy_obj_2.h>

int main(int argc, char const *argv[])
{
    auto lib1_obj = dummy_obj_1();
    auto lib2_obj = dummy_obj_2();

    std::cout <<"Hello world, object one: " << lib1_obj.getX()<< ", object two: " << lib2_obj.getX() <<std::endl;
    return 0;
}